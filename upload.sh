#!/bin/bash
rm -rf ./dist/*
source venv/bin/activate;
python -m build
python -m twine upload dist/*
