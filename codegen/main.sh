#!/bin/bash
rm -rf telectron;
MY_DIR=$(dirname "${BASH_SOURCE[0]}")
source "$MY_DIR/versions";
pip install my-pyrogram==$PYROGRAM_VERSION;
source_path=$(python -c "import pyrogram; print(pyrogram.__file__.rsplit('/', 1)[0])");
cp -r "$source_path" .
mv pyrogram telectron
cp -r "$MY_DIR/telectron" .
python "$MY_DIR/patch.py" telectron
