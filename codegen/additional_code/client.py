

class Sequences:
    def __init__(self,
                 common_pts: int = None,
                 channel_pts: Dict[int, int] = None,
                 qts: int = None,
                 seq: int = None):
        self.lock = asyncio.Lock()
        self.qts = qts
        self.seq = seq
        self.common_pts = common_pts
        self.channel_pts = ({k: v for k, v in channel_pts.items() if self.is_channel_id(k)}
                            if channel_pts else {})

    async def set_state(self, state: raw.types.updates.State):
        async with self.lock:
            self.common_pts = state.pts
            self.qts = state.qts
            self.seq = state.seq

    def get_pts(self, channel_id: int = None) -> int:
        return self.channel_pts.get(channel_id) or 1 if channel_id is not None else self.common_pts

    def set_pts(self, new_pts: int, channel_id: int = None):
        if channel_id is not None:
            if not self.is_channel_id(channel_id):
                return
            self.channel_pts[channel_id] = new_pts
        else:
            self.common_pts = new_pts

    def delete_pts(self, channel_id: int = None):
        if channel_id is not None and channel_id in self.channel_pts:
            del self.channel_pts[channel_id]
        else:
            self.common_pts = None

    @staticmethod
    def is_channel_id(channel_id) -> bool:
        return isinstance(channel_id, int) and channel_id < -1000000000000


class RequestingChats:
    NO_UPDATES_TIMEOUT = 15 * 60
    CHANNELS_CHECK_DELAY = 30

    def __init__(self, client: Client, requesting_chats: List = None):
        self.requesting_chats = ([c for c in requesting_chats if Sequences.is_channel_id(c)]
                                 if requesting_chats else [])
        self.lock = asyncio.Lock()
        self.loop = asyncio.get_event_loop()
        self.client = client
        self.worker_task = None
        self.task_event = asyncio.Event()
        self.last_check = self.last_update = self.loop.time()
        self.check_delay = self.CHANNELS_CHECK_DELAY

    async def add_requesting_chats(self, chats: List):
        async with self.lock:
            self.requesting_chats.extend([c for c in chats if Sequences.is_channel_id(c)])
            self.requesting_chats = list(set(self.requesting_chats))
            if len(self.requesting_chats) > self.check_delay:
                self.check_delay = len(self.requesting_chats)

    async def get_requesting_chats(self) -> List:
        async with self.lock:
            return self.requesting_chats.copy()

    async def delete_requesting_chat(self, chat_id: int):
        async with self.lock:
            if chat_id in self.requesting_chats:
                self.requesting_chats.remove(chat_id)

    async def start(self):
        self.worker_task = self.loop.create_task(self.worker())

    async def renew_last_update(self):
        self.last_update = self.loop.time()

    async def stop(self):
        self.task_event.set()
        if self.worker_task:
            await self.worker_task
        self.task_event.clear()

    async def worker(self):
        while True:
            now = self.loop.time()
            try:
                await asyncio.wait_for(self.task_event.wait(), timeout=min(
                    (self.last_check + self.check_delay) - now,
                    (self.last_update + self.NO_UPDATES_TIMEOUT) - now
                ))
            except asyncio.TimeoutError:
                pass
            else:
                break
            if ((self.last_check + self.check_delay) - now) <= 0:
                if not self.client.me.is_bot:
                    chats = await self.get_requesting_chats()
                    if len(chats):
                        self.loop.create_task(self.client.check_channels_difference(
                            chats, sec_per_channel=(self.check_delay / len(chats))
                        ))
                self.last_check = self.loop.time()
            if ((self.last_update + self.NO_UPDATES_TIMEOUT) - now) <= 0:
                self.loop.create_task(self.client.check_difference())
                self.loop.create_task(
                    self.client.check_channels_difference(
                        list(self.client.sequences.channel_pts.keys())
                    )
                )
                await self.renew_last_update()
