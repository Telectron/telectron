import os
import sys
import re


api_id, api_hash = 1, 'b6b154c3707471f5339bd661645ed3d6'
tab = ' '*4


def replace_file_content_decorator(filename: str):
    def decorator(func):
        def handler(file_dir: str):
            with open(file_dir + '/' + filename, 'rt') as file:
                content = file.read()
            content = func(content)
            with open(file_dir + '/' + filename, 'wt') as file:
                file.write(content)
        return handler
    return decorator


def replace_name(path: str):
    for current_path, dirs, files in os.walk(path):
        for filename in files:
            if not filename.endswith('.py'):
                continue

            @replace_file_content_decorator(filename)
            def replace_project_name(content: str) -> str:
                return content.replace('pyrogram', 'telectron').replace('Pyrogram', 'telectron')

            replace_project_name(current_path)


@replace_file_content_decorator('crypto/aes.py')
def patch_crypto_aes(content: str) -> str:
    return re.sub('''log.warning\\(
        "TgCrypto is missing! "
        "telectron will work the same, but at a much slower speed. "
        "More info: .*"
    \\)\n''', 'log.warning("TgCrypto is missing")', content) + '''\n\ntry:
    import tgcrypto_optimized

    log.info("Using tgcrypto_optimized")

    def ige256_encrypt(data: bytes, key: bytes, iv: bytes) -> bytes:
        return tgcrypto_optimized.ige256_encrypt(data, key, iv)

    def ige256_decrypt(data: bytes, key: bytes, iv: bytes) -> bytes:
        return tgcrypto_optimized.ige256_decrypt(data, key, iv)

    def ctr256_encrypt(data: bytes, key: bytes, iv: bytearray, state: bytearray = None) -> bytes:
            result, new_state, new_iv = tgcrypto_optimized.ctr256_encrypt(bytes(data), bytes(key), bytes(iv), state[0] if state else 0)
            if state is not None:
                state[0] = new_state
            iv[:] = bytearray(new_iv)
            return result

    ctr256_decrypt = ctr256_encrypt

except ImportError:
    pass\n'''


@replace_file_content_decorator('__init__.py')
def change_version(content: str) -> str:
    pyrogram_version = os.environ.get('PYROGRAM_VERSION')
    telectron_version = os.environ.get('TELECTRON_VERSION')
    return content.replace(f'__version__ = "{pyrogram_version}"',
                           f'__version__ = "{pyrogram_version}.{telectron_version}"')


@replace_file_content_decorator('methods/advanced/__init__.py')
def patch_advanced(content: str) -> str:

    return content.replace(
        '\n\nclass Advanced',
        'from .wait_for import WaitFor\n'
        'from .stream_file import StreamFile\n'
        'from .handle_download import HandleDownload\n'
        'from .handle_updates import HandleUpdates'
        '\n\n\nclass Advanced'
    ).replace(
        '\n):\n    pass',
        ',\n    StreamFile,'
        '\n    WaitFor,'
        '\n    HandleDownload,'
        '\n    HandleUpdates'
        '\n):\n    pass'
    )


@replace_file_content_decorator('methods/decorators/__init__.py')
def patch_decorators(content: str) -> str:
    return content.replace('\n\nclass Decorators',
                           'from .on_album import OnAlbum\n\n\nclass Decorators').replace(
        '\n):\n    pass',
        ',\n    OnAlbum\n):\n    pass'
    )


@replace_file_content_decorator('client.py')
def patch_client(content: str) -> str:
    content = (
        content.replace('import asyncio', 'import asyncio\nfrom base64 import b64decode')
               .replace('from typing import Union,', 'from typing import Union, Dict,')
    )
    content = re.sub(
        '([\\r\\t\\f ]*)APP_VERSION = ',

        '\\1API_ID, API_HASH = (b64decode("MTpiNmIxNTRjMzcwNzQ3MWY1MzM5YmQ2NjE2NDVlZDNkNg==")\n'
        '\\1                    .decode("utf-8").split(":"))\n'
        '\\1API_ID = int(API_ID)\n'
        '\\1APP_VERSION = ',

        content)
    tabspace = ' '*8
    content = content.replace(
        f'def __init__(\n{tabspace}self,\n{tabspace}name: str',
        f'def __init__(\n{tabspace}self,\n{tabspace}name: str = "telectron"'
    )
    content = re.sub('api_id(.*) = None', 'api_id\\1 = API_ID', content)
    content = re.sub('api_hash(.*) = None', 'api_hash\\1 = API_HASH', content)
    content = re.sub(
        f'\n{tab}\\):\n{tab*2}super\\(\\)\\.__init__\\(\\)',
        f',\n{tab*2}requesting_chats: List = None'
        f'\n{tab}):\n{tab*2}super().__init__()\n\n'
        f'{tab*2}self.albums = {{}}\n'
        f'{tab*2}self.album_lock = asyncio.Lock()\n'
        f'{tab*2}self.requesting_chats = RequestingChats(self, requesting_chats)\n'
        f'{tab*2}self.sequences = Sequences()',
        content
    )
    content = content.replace('Path(sys.argv[0]).parent', 'Path(os.getcwd())')
    content = re.sub(
        'async def handle_download.*async def get_file',
        'async def get_file',
        content, flags=re.S
    )
    content = re.sub(
        'async def handle_updates.*async def load_session',
        'async def load_session',
        content, flags=re.S
    )
    with open('codegen/additional_code/client.py', 'r') as file:
        content += file.read()
    return content


@replace_file_content_decorator('types/user_and_chats/chat.py')
def patch_chat(content: str) -> str:
    content += '''
    @property
    def full_name(self) -> str:
        """Full name of the user

        Returns:
            ``str``: On success, str is returned.
        """
        return self.first_name + ' ' + self.last_name \\
            if self.last_name \\
            else self.first_name

    @property
    def name(self) -> str:
        """title if it is a channel, supergroup or group, or full name if it is a user or bot

        Returns:
            ``str``: On success, str is returned.
        """
        return self.title or self.full_name

    def wait_for_message(self, filters=all_filter) -> str:
        """shortcut for ```client.register_waiting(
            MessageHandler,
            filters.all(chat.id) & filters
        )```

        Returns:
            ``WaitingResult``: On success, WaitingResult is returned.
        """
        return self._client.register_waiting(
            MessageHandler(None, chat_filter(self.id) & filters)
        )
'''
    content = content.replace('\n\nclass Chat(Object):',
                           '''from telectron.handlers.message_handler import MessageHandler
from telectron.filters import all as all_filter, chat as chat_filter\n\n\nclass Chat(Object):''')
    content = content.replace(
        '\n    async def unban_member(',
        '    kick_member = ban_member\n\n    async def unban_member('
    )
    return content


@replace_file_content_decorator('types/user_and_chats/user.py')
def patch_user(content: str) -> str:
    return content + '''
    @property
    def full_name(self) -> str:
        """Full_name of the chat if the chat is a user.

        Returns:
            ``str``: On success, str is returned.
        """
        return self.first_name + ' ' + self.last_name \\
            if self.last_name \\
            else self.first_name

    @property
    def name(self) -> str:
        """full name

        Returns:
            ``str``: On success, str is returned.
        """
        return self.full_name
'''


@replace_file_content_decorator('types/messages_and_media/message.py')
def patch_message(content: str) -> str:
    content = content.replace('''
                elif isinstance(media, raw.types.MessageMediaWebPage):
                    if isinstance(media.webpage, raw.types.WebPage):
                        web_page = types.WebPage._parse(client, media.webpage)
                        media_type = enums.MessageMediaType.WEB_PAGE
                    else:
                        media = None\n''', '''
                elif isinstance(media, raw.types.MessageMediaWebPage):
                    if isinstance(media.webpage, raw.types.WebPage):
                        web_page = types.WebPage._parse(client, media.webpage)
                        media_type = enums.MessageMediaType.WEB_PAGE
                    elif isinstance(media.webpage, raw.types.WebPageEmpty):
                        web_page = True
                        media_type = enums.MessageMediaType.WEB_PAGE
                    else:
                        media = None
                elif isinstance(media, raw.types.WebPageEmpty):
                    web_page = True
                    media_type = enums.MessageMediaType.WEB_PAGE\n''')
    content += '''
    @property
    def html_text(self) -> str:
        """Text unprased as html.

        Returns:
            ``str``: On success, str is returned.
        """
        return Parser.unparse(
            self.text or self.caption or '',
            self.entities or self.caption_entities or [],
            True
        )
'''
    f'{tab}):\n{tab*2}super().__init__(client)'
    content = content.replace(
        f'\n{tab}):\n{tab*2}super().__init__(client)',
        f',\n{tab*2}silent: bool = None\n{tab}):\n{tab*2}super().__init__(client)'
    ).replace(
        'self.reactions = reactions',
        f'self.reactions = reactions\n{tab*2}self.silent = silent'
    )
    content = re.sub(',\n( *)client=client',
                     ',\n\\1silent=message.silent,\n\\1client=client',
                     content)
    return content


@replace_file_content_decorator('filters.py')
def patch_filters(content: str) -> str:
    return content + '''

async def edited_filter(_, __, m: Message):
    return bool(m.edit_date)


edited = create(edited_filter)
"""Filter edited messages."""


def text_exact(text: str):
    """Filter updates that exactly match a given text.

    Can be applied to handlers that receive one of the following updates:

    - :obj:`~telectron.types.Message`: The filter will match ``text`` or ``caption``.
    - :obj:`~telectron.types.CallbackQuery`: The filter will match ``data``.
    - :obj:`~telectron.types.InlineQuery`: The filter will match ``query``.


    Parameters:
        text (``str``):
            The text.
    """

    async def func(flt, _, update: Update):
        if isinstance(update, Message):
            value = update.text or update.caption
        elif isinstance(update, CallbackQuery):
            value = update.data
        elif isinstance(update, InlineQuery):
            value = update.query
        else:
            raise ValueError(f"Regex filter doesn't work with {type(update)}")

        return bool(value == flt.t)

    return create(
        func,
        "TextExactFilter",
        t=text
    )


album_message = create(lambda _, __, m: m.media_group_id)
'''


@replace_file_content_decorator('storage/sqlite_storage.py')
def patch_sqlite_storage(content: str) -> str:
    tabspace = ' '*8
    attr = ', attr: str'
    content = content.replace('import inspect\n', '').replace(
        f'def _get(self):\n{tabspace}attr = inspect.stack()[2].function\n',
        f'def _get(self{attr}):'
    ).replace(
        f'def _set(self, value: Any):\n{tabspace}attr = inspect.stack()[2].function\n',
        f'def _set(self{attr}, value: Any):'
    ).replace(
        f'def _accessor(self, value: Any = object):\n'
        f'{tabspace}return self._get() if value == object else self._set(value)',

        f'def _accessor(self{attr}, value: Any = object):\n'
        f'{tabspace}return self._get(attr) if value == object else self._set(attr, value)'
    )
    content = re.sub(
        f'async def (\\w+)\\(self, value: (\\w+) = object\\):\n{tabspace}return self\\._accessor\\(value\\)',
        f'async def \\1(self, value: \\2 = object):\n{tabspace}return self._accessor("\\1", value)',
        content
    )
    return content


@replace_file_content_decorator('methods/chats/ban_chat_member.py')
def patch_ban_chat_member(content: str) -> str:
    return content + '\n    kick_chat_member = ban_chat_member\n'


@replace_file_content_decorator('parser/html.py')
def patch_html(content: str) -> str:
    return content.replace(
        f'{tab*2}entities_offsets = []\n\n{tab*2}for entity in entities:\n',

        f'{tab*2}entities_offsets = []\n\n{tab*2}for entity in entities:\n'
        f'{tab*3}if entity is None:\n{tab*4}continue\n'
    )


@replace_file_content_decorator('methods/chats/get_dialogs.py')
def patch_iter_dialogs(content: str) -> str:
    return content.replace(
        'if not dialogs',
        'if not dialogs or offset_date == dialogs[-1].top_message.date'
    )


@replace_file_content_decorator('methods/messages/send_media_group.py')
def patch_send_media_group(content: str) -> str:
    return content.replace(
        'disable_notification: bool = None,',

        f'file_names: List[str] = None,\n{tab*2}disable_notification: bool = None,'
    ).replace(
        'multi_media = []',

        f'if file_names is None:\n{tab*3}file_names = [None] * 10\n{tab*2}multi_media = []'
    ).replace(
        'for i in media',
        'for e, i in enumerate(media)'
    ).replace(
        'raw.types.DocumentAttributeFilename(file_name=',
        'raw.types.DocumentAttributeFilename(file_name=file_names[e] or '
    )


@replace_file_content_decorator('dispatcher.py')
def patch_dispatcher(content: str) -> str:
    return content.replace(
        'def __init__(self, client: "telectron.Client"):',
        f'def __init__(self, client: "telectron.Client"):\n{tab*2}self.waiting_events = {{}}'
    ).replace(
        'break\n            except telectron.StopPropagation:',

        '''break
                    for key, event in self.waiting_events.items():
                        if isinstance(event.handler, handler_type):
                            try:
                                if await event.handler.check(self.client, parsed_update):
                                    async with self.waiting_events[key].lock:
                                        if not self.waiting_events[key].update:
                                            self.waiting_events[key].update = parsed_update
                                            self.waiting_events[key].event.set()
                            except Exception as e:
                                log.error(e, exc_info=True)
                                continue

            except telectron.StopPropagation:'''
    )


@replace_file_content_decorator('methods/messages/edit_message_media.py')
def patch_edit_message_media(content: str) -> str:
    return content.replace(
        'isinstance(media.media, io.BytesIO)',
        'not isinstance(media.media, str)'
    ).replace(
        'mime_type=self.guess_mime_type(media.media)',
        'mime_type=(self.guess_mime_type(file_name) if file_name else self.guess_mime_type(media.media))'
    )


@replace_file_content_decorator('methods/messages/download_media.py')
def patch_download_media(content: str) -> str:
    return content.replace(' self.PARENT_DIR /', '').replace(
        'mime_type = getattr(media, "mime_type", "")',
        f'if mime_type is None:\n{tab*3}mime_type = getattr(media, "mime_type", "")'
    ).replace(
        f'\n{tab}) ->',
        f',\n{tab*2}mime_type: Optional[str] = None\n{tab}) ->')


@replace_file_content_decorator('methods/advanced/save_file.py')
def patch_save_file(content: str) -> str:
    return content.replace(
        f'{tab*2}elif isinstance(path, io.IOBase):\n{tab*3}fp = path',
        f'{tab*2}elif isinstance(path, io.IOBase):\n{tab*3}fp = path\n'
        f'{tab*2}elif hasattr(path, "url") and hasattr(path, "headers") '
        'and hasattr(path, "content"):\n'
        f'{tab*3}return await self.stream_file(path, file_id, file_part, progress, progress_args)'
    )


@replace_file_content_decorator('utils.py')
def patch_utils(content: str) -> str:
    return content.replace(
        'import struct',
        'import random\nimport string\nimport struct'
    ) + '''\n\ndef randstr(length=6) -> str:
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(length))\n'''


@replace_file_content_decorator('methods/utilities/start.py')
def patch_start(content: str) -> str:
    return content.replace(
        'await self.invoke(raw.functions.updates.GetState())',
        'await self.sequences.set_state(await self.invoke(raw.functions.updates.GetState()))'
    )


@replace_file_content_decorator('methods/auth/initialize.py')
def patch_initialize(content: str) -> str:
    return content.replace(
        'self.is_initialized = True',
        f'await self.requesting_chats.start()\n\n'
        f'{tab*2}self.is_initialized = True'
    )


@replace_file_content_decorator('methods/auth/terminate.py')
def patch_terminate(content: str) -> str:
    return content.replace(
        'await self.dispatcher.stop()',
        f'await self.requesting_chats.stop()\n'
        f'{tab*2}await self.dispatcher.stop()'
    )


def main(path: str):
    replace_name(path)
    change_version(path)
    patch_client(path)
    patch_chat(path)
    patch_user(path)
    patch_crypto_aes(path)
    patch_decorators(path)
    patch_message(path)
    patch_filters(path)
    patch_sqlite_storage(path)
    patch_ban_chat_member(path)
    patch_advanced(path)
    patch_html(path)
    patch_iter_dialogs(path)
    patch_send_media_group(path)
    patch_dispatcher(path)
    patch_edit_message_media(path)
    patch_download_media(path)
    patch_save_file(path)
    patch_utils(path)
    patch_start(path)
    patch_initialize(path)
    patch_terminate(path)


if __name__ == '__main__':
    assert len(sys.argv) >= 2, 'Specify the directory for patch'
    assert len(sys.argv) == 2, 'The script accepts only one argument'
    main(sys.argv[1])
