from telectron.raw.core.serialization import loads_tlobject, dumps_tlobject

from ..serialization import message, send_message


def test_serialize_message():
    assert message == dumps_tlobject(loads_tlobject(message))


def test_serialize_send_message():
    assert send_message == dumps_tlobject(loads_tlobject(send_message))
